﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Feature.Events.Models
{
    public class EventCalendarModel
    {
        public string EventlistId { get; set; }
        public string EventListUrl { get; set; }
    }
}