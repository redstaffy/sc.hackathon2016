#Events feature - Sitecore Hackathon 2016

We did this feature module as part of the Sitecore Hackathon 2016 challange, for the "New Habitat Feature" category.

Sc Hackathon 2016 - http://sitecorehackathon.org/sitecore-hackathon-2016/

* Intro part 1 - https://youtu.be/abs7W2kKTHs
* Intro part 2 - https://youtu.be/j0Fi62Zw_xQ


#Sitecore Habitat

Habitat is a Sitecore solution example built on a modular architecture.
The architecture and methodology focuses on:

* Simplicity - *A consistent and discoverable architecture*
* Flexibility - *Change and add quickly and without worry*
* Extensibility - *Simply add new features without steep learning curve*